#!/bin/bash

########################################################################################################################
# Log to stdout
# Arguments:
#   A string or a list of strings
# Returns:
#   None
########################################################################################################################
function log(){
	local argv="$*"

	echo "[$(date +%Y.%m.%d-%H:%M:%S) | $$ : ${MY_NAME} ] $argv"
}

log "Starting Connection Supervisor ..."

cd /opt/connection-supervisor
cp /data/options.json config/$(hostname).json
npm start
