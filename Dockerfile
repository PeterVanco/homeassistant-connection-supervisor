ARG BUILD_FROM
FROM $BUILD_FROM

ENV LANG C.UTF-8

# Install requirements for add-on
RUN apk add --no-cache curl git nodejs nodejs-npm openssh

# Copy data for add-on
COPY run.sh /
RUN chmod a+x /run.sh && \
    mkdir -p /opt/connection-supervisor
    
WORKDIR /opt/connection-supervisor
RUN git clone https://gitlab.com/PeterVanco/connection-supervisor . && \
    npm install

CMD [ "/run.sh" ]